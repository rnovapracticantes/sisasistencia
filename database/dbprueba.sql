-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-04-2018 a las 17:16:29
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbprueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id_area` int(11) NOT NULL,
  `area` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `areas`
--

INSERT INTO `areas` (`id_area`, `area`, `created_at`, `updated_at`) VALUES
(1, 'analisis de sistemas', '2018-04-17 23:01:15', '2018-04-17 23:01:15'),
(2, 'Marketing', '2018-04-17 23:01:25', '2018-04-17 23:01:25'),
(3, 'Diseño Grafico', '2018-04-17 23:01:38', '2018-04-17 23:01:38'),
(4, 'Informatica', '2018-04-17 23:01:47', '2018-04-17 23:01:47'),
(5, 'Finanzas', '2018-04-17 23:01:55', '2018-04-17 23:01:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencias`
--

CREATE TABLE `asistencias` (
  `id_asis` int(11) NOT NULL,
  `entrada` time DEFAULT NULL,
  `salida` time DEFAULT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_fecha` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asistencias`
--

INSERT INTO `asistencias` (`id_asis`, `entrada`, `salida`, `id_user`, `id_fecha`, `created_at`, `updated_at`) VALUES
(6, '15:01:30', '15:01:47', 1, 2, NULL, NULL),
(7, '15:02:00', '15:02:48', 1, 2, NULL, NULL),
(8, '15:40:59', '15:41:08', 1, 2, NULL, NULL),
(9, '15:41:40', '18:17:35', 1, 2, NULL, NULL),
(10, '10:18:57', '11:50:31', 1, 3, NULL, NULL),
(11, '11:50:45', NULL, 1, 3, NULL, NULL),
(12, '10:56:10', '13:36:22', 1, 4, NULL, NULL),
(30, '14:44:22', '14:44:22', 1, 4, NULL, NULL),
(31, '14:44:23', '14:44:23', 1, 4, NULL, NULL),
(32, '14:44:23', '14:44:23', 1, 4, NULL, NULL),
(33, '14:44:23', '14:44:24', 1, 4, NULL, NULL),
(34, '14:44:24', '14:44:24', 1, 4, NULL, NULL),
(35, '14:44:24', '14:44:57', 1, 4, NULL, NULL),
(36, '14:44:58', '14:44:58', 1, 4, NULL, NULL),
(37, '14:44:58', '14:44:58', 1, 4, NULL, NULL),
(38, '14:44:58', '14:44:59', 1, 4, NULL, NULL),
(39, '14:44:59', '14:44:59', 1, 4, NULL, NULL),
(40, '14:44:59', '14:44:59', 1, 4, NULL, NULL),
(41, '14:45:00', '14:45:00', 1, 4, NULL, NULL),
(42, '14:45:00', '14:45:00', 1, 4, NULL, NULL),
(43, '14:45:00', '14:45:01', 1, 4, NULL, NULL),
(44, '14:45:01', '14:50:44', 1, 4, NULL, NULL),
(45, '14:51:00', '14:52:14', 1, 4, NULL, NULL),
(46, '14:52:33', '14:52:51', 1, 4, NULL, NULL),
(47, '14:57:07', '14:57:30', 1, 4, NULL, NULL),
(48, '14:58:05', '14:58:09', 1, 4, NULL, NULL),
(49, '14:58:15', '14:58:50', 1, 4, NULL, NULL),
(50, '14:59:12', '15:01:44', 1, 4, NULL, NULL),
(51, '15:01:53', '15:12:05', 1, 4, NULL, NULL),
(52, '16:08:20', NULL, 1, 4, NULL, NULL),
(53, '20:27:28', '20:29:21', 5, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE `cargos` (
  `id_cargo` int(11) NOT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`id_cargo`, `cargo`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', '2018-04-17 23:00:22', '2018-04-17 23:00:22'),
(2, 'Empleado', '2018-04-17 23:00:36', '2018-04-17 23:00:36'),
(3, 'Practicante', '2018-04-17 23:00:44', '2018-04-17 23:00:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fechas`
--

CREATE TABLE `fechas` (
  `id_fecha` int(11) NOT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fechas`
--

INSERT INTO `fechas` (`id_fecha`, `fecha`) VALUES
(1, '2018-04-17'),
(2, '2018-04-19'),
(3, '2018-04-20'),
(4, '2018-04-23'),
(5, '2018-04-24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CI` int(11) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_cargo` int(11) DEFAULT NULL,
  `id_area` int(11) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `CI`, `telefono`, `remember_token`, `created_at`, `updated_at`, `id_cargo`, `id_area`, `estado`) VALUES
(1, 'dajir01', 'dajir@rnova.net', '$2y$10$AzKfGLlqiQ9pJhNUughXIOtvm5kpw2NdjQEvKrk/ZqdIYBtmOFwzq', 123456, 77410630, '0poRm47l3Xc49VLJLav34r9gOXUCBv3TpbCoL2pNtldsGUDXaUzAvvC4UIxo', '2018-04-17 00:21:37', '2018-04-26 20:30:22', 1, 1, 1),
(2, 'juan01', 'juan@gmail.com', '$2y$10$X.1gdVZAeGZbBIcDWOojUOGQN2hSdmwMyP7Ph2OIQxmIMDIz7q96u', 789456, 7755544, 'wVJnPHcBQr3oosKAoj6AkY5yDpt0DT6Pchd3j8A3uORgRFna8qjg7sNuSNhi', '2018-04-19 20:04:33', '2018-04-20 20:20:56', 1, 1, 1),
(3, 'jaunito', 'juanito@gmail.com', '$2y$10$dV5Y6cPBzA3fxqc1lRjhxOtu4ChDg6FjVggEm3pUwMcz73kx28u3q', 456123, 258, NULL, '2018-04-20 19:50:51', '2018-04-20 19:53:13', 2, 2, 0),
(4, 'juanito', 'juanito@gmail.com', '$2y$10$1qy7ZoddhKEOHPfmgMeOv.ot/ca/JMuRrlmLeRQkB1J/FlmgqHo4C', 456123, 852147, NULL, '2018-04-20 20:14:27', '2018-04-20 20:14:29', 3, 5, 0),
(5, 'boris', 'boris@gmail.com', '$2y$10$LfgSyLf2WAKWDkRUc80NNuFl3udH7NgJb4KiiWNHfmYvxLmtamP4S', 666, 911, 'L4WVLKaptrVYl3j358XraitOT1jaOuFp9U8J0fweL0pv6U4SGSdXvc1NkfsO', '2018-04-20 20:22:00', '2018-04-20 20:22:00', 2, 3, 1),
(6, 'william', 'will@gmail.com', '$2y$10$yTx2qNdvyte2Ua0e8TUXQuOy4o0FbPdxAISakNJHDr5ezmLO1cnl.', 963258, 852147, NULL, '2018-04-20 20:50:34', '2018-04-20 20:50:38', 3, 5, 0),
(7, 'jhojan01', 'jhojan@gmail.com', '$2y$10$IiMJFTWoHqLzVPIEko7FZuBOjh9MXsyokxozQ9RWZavmlkiqqE1FW', 753159, 753159, NULL, '2018-04-23 16:35:47', '2018-04-23 16:36:40', 1, 1, 0),
(8, 'saul', 'saul@gmail.com', '$2y$10$U8Akv6/JIMNGwR2hPnA/NOXdZ4vJqCKYzKU/crO69/uNA8G6jpKIu', 911, 153454, NULL, '2018-04-25 00:25:33', '2018-04-25 00:26:18', 2, 1, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id_area`);

--
-- Indices de la tabla `asistencias`
--
ALTER TABLE `asistencias`
  ADD PRIMARY KEY (`id_asis`),
  ADD KEY `fk_asistencias_users1_idx` (`id_user`),
  ADD KEY `fk_asistencias_fechas1_idx` (`id_fecha`);

--
-- Indices de la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id_cargo`);

--
-- Indices de la tabla `fechas`
--
ALTER TABLE `fechas`
  ADD PRIMARY KEY (`id_fecha`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_cargos_idx` (`id_cargo`),
  ADD KEY `fk_users_areas1_idx` (`id_area`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id_area` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `asistencias`
--
ALTER TABLE `asistencias`
  MODIFY `id_asis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT de la tabla `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id_cargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `fechas`
--
ALTER TABLE `fechas`
  MODIFY `id_fecha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asistencias`
--
ALTER TABLE `asistencias`
  ADD CONSTRAINT `fk_asistencias_fechas1` FOREIGN KEY (`id_fecha`) REFERENCES `fechas` (`id_fecha`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_asistencias_users1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_areas1` FOREIGN KEY (`id_area`) REFERENCES `areas` (`id_area`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_cargos` FOREIGN KEY (`id_cargo`) REFERENCES `cargos` (`id_cargo`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
