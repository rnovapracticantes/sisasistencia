<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::middleware('admin')->group(function () {
    Route::resource('/cargo','CargoController');
    Route::Resource('/area', 'AreaController');
    Route::get('/horas/conteo', 'UserController@conteo');
    Route::get('/horas/{id}/reset', 'UserController@reset');
    Route::Resource('users', 'UserController');
});


Auth::routes();

Route::Resource('/logintablet', 'Tablecontroller');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('horas/{id}/', 'UserController@fecha');
Route::post('horas/loadfechas/', 'UserController@loadfechas');
Route::get('fechas/print/{id}','UserController@print'); 
Route::get('fechas/edit/{id}','UserController@editfecha'); 
Route::Resource('/fecha','Tablecontroller'); 