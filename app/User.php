<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','password','CI','telefono','id_cargo', 'id_area','estado'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function cargo(){
        return $this->hasOne(cargo::class, 'id_cargo', 'id_cargo');
    }
    public function area(){
        return $this->hasOne(area::class, 'id_area', 'id_area');
    }
}
