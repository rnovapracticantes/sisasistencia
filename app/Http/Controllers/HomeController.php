<?php

namespace App\Http\Controllers;
use App\User;
use App\Asistencia;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('estado', 1)->get();
        
        foreach ($users as $key => $value) {
            $id_user = $value->id;
            $hora="0";
            $min="0";
            $seg="0";
            $idfecha="0";
            $dia="0";
            $asistencias = Asistencia::where('id_user', $id_user )->whereNotNull('salida')->get();
            foreach ($asistencias as $key => $asis) {
                $ingreso = $asis->entrada;
                $salida = $asis->salida;
                $calculo = (date("H:i:s", strtotime("00:00:00") - strtotime($ingreso) + strtotime($salida)));
                $hora = $hora + (date('H', strtotime($calculo)));
                $min = $min + (date('i', strtotime($calculo)));
                $seg = $seg + (date('s', strtotime($calculo)));
                if ($idfecha != $asis->id_fecha) {
                    $dia++;
                    $idfecha = $asis->id_fecha;
                }
            }
            // $convhoras = $min%60;
            // dd($min);
            $value->seg = $seg%60;
            $value->min = $min%60 + intdiv($seg,60);
            $value->horas = $hora + intdiv($min,60);
            $value->dias = $dia;
        } 
        return view('home', compact('users'));
    }
}
