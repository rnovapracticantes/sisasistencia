<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asistencia;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ("LoginTablet.Logintable");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $registro = DB::table('fechas')->count();
        $primero=DB::table('fechas')->select('fecha')->get();
        $fecha = DB::table('fechas')->where('id_fecha', DB::table('fechas')->max('id_fecha'))->first();

        $carbon = new \Carbon\Carbon();
        $hoy = $carbon->now();
        $hoy = $hoy->format('Y-m-d');
        $hora= $carbon->now();
        $hora = $hora->toTimeString();
        //$hora = $hora->format('h:i:s');///modificar hora en formato 24hrs

        //busca el id del usuario
        $user = DB::table('users')->where('CI', $request->valor_numero)->first();
        //verificar si usuario existe
        if (empty($user)) {
            //si quieres pones mensaje de que el ci eta mal
            Alert::error('Incorrecto', 'Carnet')->autoclose(2000);
            return redirect ('/logintablet');
        } 
        //entra con la ultima fecha registrada en la tabla fechas
        if($registro == 0 ){
            DB::table('fechas')->insert([
                'fecha' => $hoy,
            ]);
            DB::table('asistencias')->insert([
                'entrada' => $hora,
                'id_user' => $user->id,
                'id_fecha'=> $primero->id_fecha,
                ]);
                Alert::success('Carnet', 'Correcto Ingreso' )->autoclose(2000);
                return redirect ('/logintablet');
        }
        else{
            
            if($fecha->fecha == $hoy){            
                $insertado = DB::table('asistencias')->where('id_fecha', $fecha->id_fecha)->where('id_user', $user->id)->whereNotNull('salida')->count();
                if ($insertado < 2 ) {
                //BUSCAR EN ASISTENCIA ID_FECHA Y ID_USER
                $asis = DB::table('asistencias')->where('id_fecha', $fecha->id_fecha)->where('id_user', $user->id)->get();
                if (empty($asis)) {
                    //NUEVO REGITRO DE ASISTENCIA
                    DB::table('asistencias')->insert([
                        'entrada' => $hora,
                        'id_user' => $user->id,
                        'id_fecha'=> $fecha->id_fecha,
                    ]);
                }
                else{
                    $i = 0;
                    foreach ($asis as $as) {
                        if (empty($as->salida)) {//si el campo esta vacio
                            //hacer un update con la hora de salida
                            DB::table('asistencias')->where('id_asis', $as->id_asis)->update(array(
                                'salida'=> $hora,
                            ));
                            Alert::success('Carnet', 'Correcto salida')->autoclose(2000);
                            return redirect ('/logintablet');
                        }
                        else{
                            $i++;
                            if ($i == count($asis)) {
                                //registrar ingreso en asistencia
                                DB::table('asistencias')->insert([
                                    'entrada' => $hora,
                                    'id_user' => $user->id,
                                    'id_fecha'=> $fecha->id_fecha,
                                ]);
                                Alert::success('Carnet', 'Correcto Ingreso' )->autoclose(2000);
                                return redirect ('/logintablet');
                            }
                        }
                    }

                    //registrar ingreso en asistencia
                    DB::table('asistencias')->insert([
                        'entrada' => $hora,
                        'id_user' => $user->id,
                        'id_fecha'=> $fecha->id_fecha,
                    ]);
                }
                Alert::success('Carnet', 'Correcto Ingreso' )->autoclose(2000);
                return redirect ('/logintablet');
                }
                else{
                Alert::info('Alcanzaste', 'el limite de registro' )->autoclose(2000);
                return redirect ('/logintablet');
                }
            }
            else{
                //ingresar la fecha actual de hoy
                DB::table('fechas')->insert([
                    'fecha' => $hoy,
                ]);
                //sacar el ultimo id_fecha
                $fecha = DB::table('fechas')->where('id_fecha', DB::table('fechas')->max('id_fecha'))->first();
                //despues ingresar en asistencia
                //NUEVO REGITRO DE ASISTENCIA
                DB::table('asistencias')->insert([
                    'entrada' => $hora,
                    'id_user' => $user->id,
                    'id_fecha'=> $fecha->id_fecha,
                ]);
                Alert::success('Carnet', 'Correcto Ingreso' )->autoclose(2000);
                //mensaje de ingrado al sistema
                return redirect ('/logintablet');
                }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Asistencia::where('id_asis', $id)->update(array(
            'entrada'=> $request->entrada,
            'salida'=> $request->salida,
        ));
        $id_usu = Asistencia::where('id_asis', $id)->first();
        $id_usuario = $id_usu->id_user;
        return view('horas.fecha', compact('id_usuario'));
        

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
