<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Cargo;
use App\Area;
use App\Asistencia;
use App\Fecha;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        return view ('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cargos = cargo::get();
        $areas = area::get();
        return view ('users.create', compact('cargos', 'areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::Create([
            'name' => $request['name'],
            'email' => $request['email'],
            'CI' => $request['CI'],
            'password' => bcrypt($request['CI']),
            'telefono' => $request['telefono'],
            'estado' => 1,
            'id_area' => $request['id_area'],
            'id_cargo' => $request['id_cargo']
        ]);
        return redirect ('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = user::where('id', $id)->firstOrFail();
        
       return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        user::where('id', $id)->update(array(
            'name'=> $request->name,
            'email'=> $request->email,
            'telefono'=> $request->telefono,
            'id_area' => $request->id_area,
            'id_cargo' => $request->id_cargo,
            ));
        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->update(['estado' =>0]);
        return redirect('/users');
    }

    public function conteo()
    {
        $users = User::where('estado',1)->get();
        
        foreach ($users as $key => $value) {
            $id_user = $value->id;
            $hora="0";
            $min="0";
            $seg="0";
            $idfecha="0";
            $dia="0";
            $asistencias = Asistencia::where('id_user', $id_user )->whereNotNull('salida')->get();
            foreach ($asistencias as $key => $asis) {
                $ingreso = $asis->entrada;
                $salida = $asis->salida;
                $calculo = (date("H:i:s", strtotime("00:00:00") - strtotime($ingreso) + strtotime($salida)));
                $hora = $hora + (date('H', strtotime($calculo)));
                $min = $min + (date('i', strtotime($calculo)));
                $seg = $seg + (date('s', strtotime($calculo)));
                if ($idfecha != $asis->id_fecha) {
                    $dia++;
                    $idfecha = $asis->id_fecha;
                }
            }
            // $convhoras = $min%60;
            // dd($min);
            $value->seg = $seg%60;
            $value->min = $min%60 + intdiv($seg,60);
            $value->horas = $hora + intdiv($min,60);
            $value->dias = $dia;
        } 
        return view ('horas.horas', compact('users'));
    }
    public function fecha ( $id ){
        $id_usuario = $id;
        return view('horas.fecha', compact('id_usuario'));
    }

    public function loadfechas ( Request $request ){
        $desde = date("Y-m-d");
        $hasta = date("Y-m-d");
        $id_user = 1;
        if ($request) {
            $desde = $request->desde;
            $hasta = $request->hasta;
            $id_user = $request->id_user;
        }

        $asistencias = DB::select(
            "select asistencias.* from asistencias INNER JOIN fechas ON asistencias.id_fecha = fechas.id_fecha where asistencias.id_user = :id_user AND fechas.fecha >= :desde AND fechas.fecha <= :hasta  ", 
            [
                'id_user' => $id_user,   
                'desde' => $desde,
                'hasta' => $hasta
            ]);

        foreach ($asistencias as $key => $value) {
            $fecha = Fecha::where('id_fecha', $value->id_fecha )->first();
            $value->nombfecha = $fecha->fecha;
        }
        return response($asistencias, 200)
                  ->header('Content-Type', 'text/plain');
    }
    public function print ($id){
        $asistencias = Asistencia::where('id_user', $id)->get();
        foreach ($asistencias as $key => $value) {
            $fecha = Fecha::where('id_fecha', $value->id_fecha )->first();
            $user = User::where('id', $value->id_user)->first();
            $value->nombfecha = $fecha->fecha;
            $value->nombre = $user->name;
        }
        $nombre = $asistencias[0]['nombre'];

        $users = User::where('id', $id)->get();;
        
        foreach ($users as $key => $value) {
            $hora="0";
            $min="0";
            $seg="0";
            $idfecha="0";
            $dia="0";
            $asistencia2 = Asistencia::where('id_user', $value->id )->whereNotNull('salida')->get();
            foreach ($asistencia2 as $key => $asis) {
                $ingreso = $asis->entrada;
                $salida = $asis->salida;
                $calculo = (date("H:i:s", strtotime("00:00:00") - strtotime($ingreso) + strtotime($salida)));
                $hora = $hora + (date('H', strtotime($calculo)));
                $min = $min + (date('i', strtotime($calculo)));
                $seg = $seg + (date('s', strtotime($calculo)));
                if ($idfecha != $asis->id_fecha) {
                    $dia++;
                    $idfecha = $asis->id_fecha;
                }
            }
            // $convhoras = $min%60;
            // dd($min);
            $value->seg = $seg%60;
            $value->min = $min%60 + intdiv($seg,60);
            $value->horas = $hora + intdiv($min,60);
            $value->dias = $dia;
        }
        // return $users;
        return view('horas.fechaprint', compact('asistencias','nombre', 'users'));
    }
    public function editfecha($id_fecha){
        $fecha = Asistencia::where('id_asis',$id_fecha)->first();
        // return $fecha;
        return view('horas.edit', compact('fecha'));
    }
    public function updateeditfecha(Request $request, $id){
        return $request;
    }
    public function reset ($id_user)
    {
        Asistencia::where('id_user',$id_user)->delete();
        return redirect('/horas/conteo');
    }
}
