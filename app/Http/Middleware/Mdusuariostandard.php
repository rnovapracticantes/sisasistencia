<?php

namespace App\Http\Middleware;

use Closure;

class Mdusuariostandard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario_actual = \auth::user();
        if ($usuario_actual->id_cargo!=1) {
           return view ("mensajes.msj_rechazado")->with("msj","no tienes suficientes privilegios para esta seccion");
        }
        return $next($request);
    }
}
