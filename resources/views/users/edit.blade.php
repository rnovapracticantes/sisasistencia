@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body"> -->
                 <div class="panel panel-primary">
                <div class="panel-heading">Formulario de Registro</div>
                 <div class="panel-body"><center><h3>Editar Usuario</h3></center></div>
                    <form class="form-horizontal" method="POST" action="/users/{{ $user->id }}">
                    <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}

                        
                        
<!--       Nombre completo-->
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre Completo</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

<!--        E-Mail address-->
                        
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
<!--                Telefono        -->
                        
                        
                          <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                            <label for="telefono" class="col-md-4 control-label">Telefono</label>

                            <div class="col-md-6">
                                <input id="telefono" type="text" class="form-control" name="telefono" value="{{ $user->telefono }}" required>

                                @if ($errors->has('telefono'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                                @endif
                            </div>
                         </div>
                        

                        
                        <div class="form-group{{ $errors->has('id_area') ? ' has-error' : '' }}">
                            <label for="id_area" class="col-md-4 control-label">Area</label>

                            <div class="col-md-6">
                                <!-- <input id="id_area" type="text" class="form-control" name="id_area" value="{{ $user->id_area }}" required> -->
                                <select id="id_area"  name="id_area" required="required" class="form-control">
                                <option value="1" selected>Analisis de Sistemas</option> 
                                <option value="2">Marketing</option>   
                                <option value="3">Diseño Grafico</option>   
                                <option value="4">Informatica</option> 
                                <option value="5">finanzas</option>     
                                </select>

                                @if ($errors->has('id_area'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_area') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('id_cargo') ? ' has-error' : '' }}">
                            <label for="id_cargo" class="col-md-4 control-label">id_cargo</label>

                            <div class="col-md-6">
                              <!--   <input id="id_cargo" type="text" class="form-control" name="id_cargo" value="{{ $user->id_cargo }}" required> -->

                               <select id="id_cargo"  name="id_cargo" required="required" class="form-control">
                                <option value="1" selected>Administrador</option> 
                                <option value="2">Empleado</option>   
                                <option value="3">Practicante</option>
                                <option value="4">limpieza</option>    
                                </select>

                                @if ($errors->has('id_cargo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_cargo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
