@extends('layouts.app')

@section('content')
<center><a href="/users/create" class="btn btn-primary" role="button">Nuevo Usuario</a></center>
<hr>
<div class="container">
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                
                    <th>Nombre Completo</th>
                  	<th>E-Mail</th>
                  	<th>Telefono</th>
                    <th>Area</th>
                    <th>Cargo</th>
                    <th>Activo</th>
                    <th>Acciones</th>

                </tr>
            </thead>

            <tbody>
            	@foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->telefono }}</td>
                    <td>{{ $user->area->area }}</td>
                    <td>{{ $user->cargo->cargo }}</td>
                    <td>{{ $user->estado==1?'Activo':'Inactivo'  }}</td>

                    <td>
                        <form action="/users/{{ $user->id }}" method="POST">
                           <input type="hidden" name="_method" value="DELETE">
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           
                          <button type="submit" class="btn btn-danger btn-sm">
                               <i class="far fa-trash-alt"></i>
                           </button>

                          <a href="{{ url('/users/'.$user->id.'/edit') }}" class="btn btn-primary btn-sm " role="button" data-toggle="tooltip" data-placement="right" title="editar">
                               <i class="far fa-edit"></i>
                           </a>
                       </form>    
 
                    </td>
                </tr>
        		@endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
