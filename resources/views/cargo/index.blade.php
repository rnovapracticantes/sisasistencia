@extends('layouts.app')

@section('content')
<center><a href="/cargo/create" class="btn btn-primary" role="button">Nuevo Cargo</a></center>
<hr>

<div class="container">
   
        
        <table class="table table-bordered table-hover table-striped">
            
             <thead class="thead-inverse">
                
             
                <tr>
                    <th>id cargo</th>
                    <th>Cargo que pertenece</th>
                    <th>Accion a realizar</th>
                  
               
                </tr>
             
            </thead>
            
            <tbody>
                @foreach($cargos as $cargo)
                <tr>
                    
                    <td>{{ $cargo->id_cargo }}</td>
                    <td>{{ $cargo->cargo}}</td>
                    
                    <td>

                
                    <form action="/cargo/{{ $cargo->id_cargo }}" method="POST">
                           <input type="hidden" name="_method" value="DELETE">
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           
                          <button type="submit" class="btn btn-danger btn-sm">
                               <i class="far fa-trash-alt"></i>
                           </button>

                          <a href="{{ url('/cargo/'.$cargo->id_cargo.'/edit') }}" class="btn btn-primary btn-sm " >
                               <i class="far fa-edit"></i>
                           </a>
                    </form>
                
                @endforeach
            </tbody>
        </table>
        
    
</div>

@endsection
