@extends('layouts.app')

@section('content')
<center><h3>EDITAR CARGO<h3></center>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


     
                <div class="panel panel-primary">
                <div class="panel-heading">Editar</div>
                 <div class="panel-body"></div>
                    
                    <form class="form-horizontal" method="POST" action="/cargo/{{$cargo->id_cargo}}">
                        <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}
<!--       Informacion de Cargo-->
                        
                       <div class="form-group{{ $errors->has('cargo') ? ' has-error' : '' }}">
                            <label for="cargo" class="col-md-4 control-label">Cargo</label>

                            <div class="col-md-6">
                                <input id="cargo" type="text" class="form-control" name="cargo" value="{{$cargo->cargo}}" required autofocus>

                                @if ($errors->has('cargo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cargo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 


                    
                            
                          <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                            
                                <button type="submit" class="btn btn-primary">Aceptar</button>
                            
                            </div>
                        </div>

                    </form>
                    </div>
                </div>
               


            </div>
        </div>

                      
                    
     
@endsection