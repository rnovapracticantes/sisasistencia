@extends ('layouts.applogin')
@section('content')
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>

  <div class="container">
    <div class="row">
      <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
      <div class="col">
      </div>
      <div class="col-xs-12 col-sm-8 col-md-12 col-lg- col-xl-8">
        <form class="Form-group" action="/logintablet" method="post" onsubmit="bloquear()">
          @include('sweetalert::alert')
          {{ csrf_field()}}
            <label for="">Inserte su numero de carnet</label>
            <input type="text" id="valor_numero" class="form-control" name="valor_numero" autocomplete="off" autofocus="true">
          <!--Numeros del 1 al 3-->
          <br>
          <div class="row">
            <div class="col">
              <button type="button" class="btn btn-primary btn-lg btn-block" value="1" onclick="numero('1')">1</button>
            </div>
            <div class="col">
              <button type="button" class="btn btn-primary btn-lg btn-block" value="2" onclick="numero('2')">2</button>
            </div>
            <div class="col">
              <button type="button" class="btn btn-primary btn-lg btn-block" value="3" onclick="numero('3')">3</button>
            </div>
          </div>
          <!--Numeros del 4 al 6-->
          <br>
          <div class="row">
            <div class="col">
              <button type="button" class="btn btn-primary btn-lg btn-block" value="4" onclick="numero('4')">4</button>
            </div>
            <div class="col">
              <button type="button" class="btn btn-primary btn-lg btn-block" value="5" onclick="numero('5')">5</button>
            </div>
            <div class="col">
              <button type="button" class="btn btn-primary btn-lg btn-block" value="6" onclick="numero('6')">6</button>
            </div>
          </div>
          <!-- Numeros del 7 al 9-->
          <br>
          <div class="row">
            <div class="col">
              <button type="button" class="btn btn-primary btn-lg btn-block" value="7" onclick="numero('7')">7</button>
            </div>
            <div class="col">
              <button type="button" class="btn btn-primary btn-lg btn-block" value="8" onclick="numero('8')">8</button>
            </div>
            <div class="col">
              <button type="button" class="btn btn-primary btn-lg btn-block" value="9" onclick="numero('9')">9</button>
            </div>
          </div>
          <!--ingreso y salida-->
          <br>
          <div class="row">
            <div class="col">
              <button type="button" class="btn btn-danger btn-lg btn-block" value="borrar" onclick="borrartotal()"><i class="fa fa-eraser fa-1x" aria-hidden="true"></i></button>
            </div>
            <div class="col">
              <button type="button" class="btn btn-primary btn-lg btn-block" value="0" onclick="numero('0')">0</button>
            </div>
            <div class="col">
              <button id="boton" type="submit" class="btn btn-success btn-lg btn-block" value="Aceptar" >Aceptar</button>
            </div>
          </div>
        </form>
      </div>
      <div class="col">
      </div>
    </div>
  </div>  
@endsection
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
	@if(Session::has('message'))
		var type="{{Session::get('alert-type','info')}}"

		switch(type){
			case 'info':
		         toastr.info("{{ Session::get('message') }}");
		         break;
	        case 'success':
	            toastr.success("{{ Session::get('message') }}");
	            break;
         	case 'warning':
	            toastr.warning("{{ Session::get('message') }}");
	            break;
	        case 'error':
		        toastr.error("{{ Session::get('message') }}");
		        break;
		}
	@endif
</script>

<script>
        //Declaracion de variables
	        var num = 0;
        //Función que coloca el número presionado
        function numero(numero){
            if(num==0 && num !== '0.'){
                num = numero;
            }else{
                num += numero;
            }
            refrescar();
        }
        //Función con la que se borra el ultimo dijito insertado
        function borrartotal(){
            num = num.slice(0, -1);
            refrescar();
        }       
        function refrescar(){
            document.getElementById("valor_numero").value = num;
        }
        function bloquear(){
            document.getElementById("boton").disabled = true;
        }
        
    </script>