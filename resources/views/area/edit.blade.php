@extends('layouts.app')

@section('content')
<center><h3>EDITAR AREA<h3></center>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


     
                <div class="panel panel-primary">
                <div class="panel-heading">Editar</div>
                 <div class="panel-body"></div>
                    
                    <form class="form-horizontal" method="POST" action="/area/{{$area->id_area}}">
                        <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}
<!--       Informacion de Cargo-->
                        
                       <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }}">
                            <label for="area" class="col-md-4 control-label">Area</label>

                            <div class="col-md-6">
                                <input id="area" type="text" class="form-control" name="area" value="{{$area->area}}" required autofocus>

                                @if ($errors->has('area'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('area') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 


                    
                            
                          <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                            
                                <button type="submit" class="btn btn-primary">Aceptar</button>
                            
                            </div>
                        </div>

                    </form>
                    </div>
                </div>
               


            </div>
        </div>

                    
     
@endsection