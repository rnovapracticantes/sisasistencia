@extends('layouts.app')

@section('content')
<center><a href="/area/create" class="btn btn-primary" role="button">Nuevo Area</a></center>
<hr>

<div class="container">
   
        
        <table class="table table-bordered table-hover table-striped">
            
             <thead class="thead-inverse">
                
             
                <tr>
                    <th>id area</th>
                    <th>Area que pertenece</th>
                    <th>Accion a realizar</th>
                  
               
                </tr>
             
            </thead>
            
            <tbody>
                @foreach($areas as $area)
                <tr>
                    
                    <td>{{ $area->id_area }}</td>
                    <td>{{ $area->area}}</td>
                    
                    <td>

                
                    <form action="/area/{{ $area->id_area }}" method="POST">
                           <input type="hidden" name="_method" value="DELETE">
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           
                          <button type="submit" class="btn btn-danger btn-sm">
                               <i class="far fa-trash-alt"></i>
                           </button>

                          <a href="{{ url('/area/'.$area->id_area.'/edit') }}" class="btn btn-primary btn-sm " >
                               <i class="far fa-edit"></i>
                           </a>
                    </form>
                
                @endforeach
            </tbody>
        </table>
        
    
</div>

@endsection
