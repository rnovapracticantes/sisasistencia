@extends ('layouts.app')
@section('content')

<center><h1><strong><div style="font-size:50px">Fechas y Horarios Asistidos</div></strong></h1></center>
<center><h1><strong><div style="font-size:30px">Nombre : {{$nombre}}</div></strong></h1></center>
@foreach($users as $user)
    <h1><strong><div align="right" style="font-size:20px">  Horas Realizadas: {{$user->horas}}</div></strong></h1>
    <h1><strong><div align="right" style="font-size:20px">  Dias Asistidos: {{$user->dias}}</div></strong></h1>
@endforeach
<div class = "container">
    <div class= "table table-responsive">
        <table class="table table-bordered table-hover table-striped" >
            <thead class="thead-inverse table-sm">
                <tr>
                    <th>Fecha</th>
                    <th>Ingreso</th>
                    <th>Salida</th>
                </tr>
            </thead>
            <tbody>
            @foreach($asistencias as $asistencia)
             <tr>
                <td>{{ $asistencia->nombfecha }}</td>
                <td>{{ $asistencia->entrada }}</td>
                <td>{{ $asistencia->salida }}</td>
             </tr>
             @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
