@extends ('layouts.app')
@section('content')
<center><h1><strong><div style="font-size:50px">Asistencias</div></strong></h1></center>
<div class = "container">
    <div class= "table table-responsive">
        <table class="table table-bordered table-hover table-striped" >
            <thead class="thead-inverse">
                <tr>
                    <th>Nombre Completo</th>
                    <th>Area</th>
                    <th>Cargo</th>
                    <th>Dias</th>
                    <th>Horas</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                @foreach($users as $user)
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->area->area }}</td>
                    <td>{{ $user->cargo->cargo }}</td>
                    <td>{{ $user->dias }}</td>
                    <td>{{ $user->horas.':'.$user->min.':'.$user->seg }}</td>
                    <td>
                        <form action="/horas/{{ $user->id }}" method="POST">
                        <a href="{{ url('/horas/'.$user->id .'') }}" class="btn btn-success btn-sm">
                            <i class="far fa-calendar-alt fa-lg"></i>
                        </a>
                        <a href="{{ url('/horas/'.$user->id.'/reset') }}" class="btn btn-primary btn-sm " >
                            <i class="fas fa-trash-alt"></i>
                        </a>
            	        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection