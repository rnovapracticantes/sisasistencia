@extends ('layouts.app')
@section('content')
<center><h1><strong><div style="font-size:50px">Fechas</div></strong></h1></center>
<div class = "container">
    <div class= "table table-responsive">
        <input type="hidden" id="id-usuario" value="<?=$id_usuario?>">
        <form id="frm-fecha">
            <table align = "center">
                <tr>
                    <td><input name="desde" class="form-control"  value="<?= date("Y-m-d")?>" type="date" id="id_fechadesde"></td>
                    <td> Hasta </td>
                    <td><input name="hasta" class="form-control" value="<?= date("Y-m-d")?>" type="date" id="id_fechahasta"></td>
                    <td>
                        <button type="submit" class="btn btn-success btn-sm" >Buscar</button>
                    </td>
                    <td>
                        <a href="/fechas/print/{{ $id_usuario }}" class="btnprn btn" ><i class="fas fa-print"></i> Imprimir</a>
                    </td>
                </tr>
            </table>
        </form>
        <table class="table table-bordered table-hover table-striped" >
            <thead class="thead-inverse">
            
                <tr>
                    <th>Fecha</th>
                    <th>Ingreso</th>
                    <th>Salida</th>
                    <th>Accion</th>
                </tr>
            </thead>
            <tbody id="dataprovider">
             
            </tbody>
        </table>
    </div>
</div>
@endsection

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/core.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script>
$(function(){
    var ajaxpetition = null;

    function loadfechas() {
        // e.preventDefault();
        var desde = $("#id_fechadesde").val() ;
        var hasta = $("#id_fechahasta").val() ;
        $('#dataprovider').html("");
        if (ajaxpetition) {
            ajaxpetition.abort();
        }
        ajaxpetition = $.ajax( {
            beforeSend: function (xhr) {
                $("#loading").css("display", "block");
            },
            success: function (data, textStatus, jqXHR) {
                $("#id_fechadesde").val( desde );
                $("#id_fechahasta").val( hasta );
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        $('#dataprovider').append("<tr> \
                                <td>"+ data[i].nombfecha + "</td> \
                                <td>"+ data[i].entrada + "</td>\
                                <td>"+ data[i].salida + "</td>\
                                <td><a href='/fechas/edit/"+data[i].id_asis+"' class='btn btn-success' role='button'>Editar</a></td>\
                            </tr>");
                    };
                } else {
                    $("#dataprovider").append("<tr>\
                            <td class='text-center' colspan='4'>No se encontro resultados</td>\
                            </tr>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == "403") {
                    location.reload();
                }
            },
            complete: function (jqXHR, textStatus) {
                $("#loading").css("display", "none");
            },
            type: 'POST',
            dataType: 'JSON',
            url: "/horas/loadfechas/",
            cache: false,
            data: {
                id_user:$("#id-usuario").val(),
                desde:desde,
                hasta:hasta,
                 _token: "{{ csrf_token() }}",
            }
        });
    }

    loadfechas();
    $('#frm-fecha').on('submit', function (e) {
        e.preventDefault();
        loadfechas();
        $('#frm-fecha')[0].reset();
    });
});
</script>
<script>
    $(document).ready(function(){
        $('.btnprn').printPage();
    });

</script>
