@extends ('layouts.app')
@section('content')
<div class="container">

    <form method="POST" action="/fecha/{{$fecha->id_asis}}">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('entrada') ? ' has-error' : '' }}">
            <label for="entrada" class="col-md-4 control-label">Entrada</label>
            <div class="col-md-6">
            <input id="entrada" type="time" placeholder="hrs:mins" class="form-control" name="entrada" value="{{$fecha->entrada}}" required autofocus>
                @if ($errors->has('entrada'))
                    <span class="help-block">
                        <strong>{{ $errors->first('entrada') }}</strong>
                    </span>
                @endif
            </div>
        </div> 

        <div class="form-group{{ $errors->has('salida') ? ' has-error' : '' }}">
            <label for="salida" class="col-md-4 control-label">Salida</label>
            <div class="col-md-6">
            <input id="salida" type="time" placeholder="hrs:mins" class="form-control" name="salida" value="{{$fecha->salida}}" required autofocus>
                @if ($errors->has('salida'))
                    <span class="help-block">
                        <strong>{{ $errors->first('salida') }}</strong>
                    </span>
                @endif
            </div>
        </div> 
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Aceptar
                </button>
            </div>
        </div>
    </form>
</div>
@endsection