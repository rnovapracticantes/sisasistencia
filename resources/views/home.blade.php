@extends('layouts.app')

@section('content')
<center><h1><strong><div style="font-size:50px">"Bienvenido"</div></strong></h1></center>
<div class = "container">
    <div class= "table table-responsive">
        <table class="table table-bordered table-hover table-striped" >
            <thead>
                <tr>
                    <th>Nombre Completo</th>
                    <th>Area</th>
                    <th>Cargo</th>
                    <th>Dias</th>
                    <th>Horas</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                @foreach($users as $user)
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->area->area }}</td>
                    <td>{{ $user->cargo->cargo }}</td>
                    <td>{{ $user->dias }}</td>
                    <td>{{ $user->horas.':'.$user->min.':'.$user->seg }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
