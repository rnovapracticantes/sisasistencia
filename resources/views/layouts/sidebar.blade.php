<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                <img src="/img/imagen.jpg" alt="person" class="img-fluid rounded-circle">
                <h2 class="h5">{{ auth()->user()->name }}</h2>
                <span>{{ auth()->user()->id_cargo==1?'Administrador':(auth()->user()->id_cargo==2?'Empleado':'Pacticante')}}</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo">
                <a href="index.html" class="brand-small text-center"> 
                    <strong>S</strong>
                    <strong class="text-primary">I</strong>
                </a>
            </div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <h5 class="sidenav-heading">Menu</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li>
                    <a href="/users"> 
                        <i class="fa fa-users"></i> Usuarios                             
                    </a>
                </li>
                <li>
                    <a href="/cargo"> 
                        <i class="fa fa-address-card"></i> Cargo                             
                    </a>
                </li>
                <li>
                    <a href="/area"> 
                        <i class="fab fa-black-tie"></i> Area                           
                    </a>
                </li>
                <li>
                    <a href="/horas/conteo"> 
                        <i class="far fa-clock"></i> Horas                           
                    </a>
                </li>
            
                    <!-- <li>
                        <a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> 
                            <i class="fa fa-stop"></i>Sanciones 
                        </a>
                        <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                            <li>
                                <a href="/sancion"> 
                                    <i class="fa fa-exclamation-triangle"></i>Jugadores                             
                                </a>
                            </li>
                            <li>
                                <a href="/deuda"> 
                                    <i class="fa fa-exclamation-triangle"></i>Equipos                             
                                </a>
                            </li>
                        </ul>
                    </li> -->
            </ul>
        </div>
        <!-- <div class="admin-menu">
            <h5 class="sidenav-heading">Second menu</h5>
            <ul id="side-admin-menu" class="side-menu list-unstyled">
                <li> 
                    <a href="/cancha"> 
                        <i class="fa fa-home"> </i>Canchas
                    </a>
                </li>
                <li> 
                    <a href="/reporte"> 
                        <i class="fa fa-home"> </i>Reportes
                    </a>
                </li>
                <li> 
                    <a href="/user"> 
                        <i class="fa fa-home"> </i>Usuarios
                    </a>
                </li>
            </ul>
        </div> -->
    </div>
</nav>  